import sbt._
import Keys._
import play.Project._

object ApplicationBuild extends Build {

  val appName         = "appTest"
  val appVersion      = "1.0-SNAPSHOT"

  val appDependencies = Seq(
    // Add your project dependencies here,
    javaCore,
    javaJdbc,
    javaEbean,
    "plugin" % "plugin_2.10" % "1.0-SNAPSHOT"
  )

   val main = PlayProject(appName, appVersion, appDependencies, mainLang = JAVA).settings(
    resolvers += "Local Play Repository" at "file:///D:/play-2.1.5/repository/local"
  )
}